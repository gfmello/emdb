package gfmello.emdb.test.integrated;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;

import gfmello.emdb.person.Person;
import gfmello.emdb.person.PersonService;
import java.util.Optional;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.Test;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PersonServiceUpdateTest extends AbstractTestNGSpringContextTests {

  @Autowired
  private PersonService service;

  /**
   * Test new person creation.
   */
  @Test(priority = 10)
  public void testSavePerson() {
    Person newPerson = new Person();
    newPerson.setFirstName("Bruce");
    newPerson.setLastName("Willis");
    newPerson.setAge(50);
    newPerson.setFavouriteColour("Red");
    Person expectedPerson = service.save(newPerson);
    assertNotNull(expectedPerson.getIdPerson());
  }

  /**
   * Test update previous person created.
   */
  @Test(priority = 20)
  public void testUpdatePerson() {
    Person previousPerson = service.findAll().get(0);
    previousPerson.setFirstName("Mad");
    previousPerson.setLastName("Max");
    previousPerson.setAge(55);
    previousPerson.setFavouriteColour("Yellow");
    Person updatePerson = service.save(previousPerson);
    
    assertEquals(updatePerson.getIdPerson(), previousPerson.getIdPerson());
    assertEquals(updatePerson.getFirstName(), "Mad");
    
  }

  /**
   * Test delete person .
   */
  @Test(priority = 30)
  public void testDeletePerson() {
    Person newPerson = new Person();
    newPerson.setFirstName("John");
    newPerson.setLastName("Rambo");
    newPerson.setAge(50);
    newPerson.setFavouriteColour("Red");
    newPerson = service.save(newPerson);
    Long idExistingPerson = newPerson.getIdPerson();

    //assert person is created
    assertNotNull(newPerson.getIdPerson());
 
    //assert person is found 
    Optional<Person> optPersonFound = service.findById(idExistingPerson);
    assertTrue(optPersonFound.isPresent());
    
    //delete person
    service.deleteById(idExistingPerson);

    //assert person is not found anymore
    optPersonFound = service.findById(idExistingPerson);
    assertFalse(optPersonFound.isPresent());
  }

  
}
