package gfmello.emdb.test.integrated;

import static org.testng.Assert.assertNotNull;

import gfmello.emdb.person.Person;
import gfmello.emdb.person.PersonService;
import javax.validation.ConstraintViolationException;
import net.bytebuddy.utility.RandomString;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.Test;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PersonServiceAddTest extends AbstractTestNGSpringContextTests {

  @Autowired
  private PersonService service;

  /**
   * Test new person creation.
   */
  @Test(priority = 10)
  public void testSavePerson() {
    Person newPerson = new Person();
    newPerson.setFirstName("Guilherme");
    newPerson.setLastName("Mello");
    newPerson.setAge(38);
    newPerson.setFavouriteColour("Blue");
    Person expectedPerson = service.save(newPerson);
    assertNotNull(expectedPerson.getIdPerson());
  }

  /**
   * Teste creating the same person as before, should raise error.
   */
  @Test(priority = 20, expectedExceptions = DataIntegrityViolationException.class)
  public void testAddRepeatedPerson() {
    Person newPerson = new Person();
    newPerson.setFirstName("Guilherme");
    newPerson.setLastName("Mello");
    newPerson.setAge(38);
    newPerson.setFavouriteColour("Red");
    Person expectedPerson = service.save(newPerson);
    assertNotNull(expectedPerson.getIdPerson());
  }
  
  /**
   * Test creating person with long name.
   */
  @Test(priority = 30, expectedExceptions = ConstraintViolationException.class)
  public void testLongName() {
    Person newPerson = new Person();
    newPerson.setFirstName(RandomString.make(300));
    newPerson.setLastName("Mello");
    newPerson.setAge(38);
    newPerson.setFavouriteColour("Red");
    Person expectedPerson = service.save(newPerson);
    assertNotNull(expectedPerson.getIdPerson());
  }

  /**
   * Test creating person with short name.
   */
  @Test(priority = 33, expectedExceptions = ConstraintViolationException.class)
  public void testShortName() {
    Person newPerson = new Person();
    newPerson.setFirstName("");
    newPerson.setLastName(RandomString.make(50));
    newPerson.setAge(38);
    newPerson.setFavouriteColour(RandomString.make(20));
    Person expectedPerson = service.save(newPerson);
    assertNotNull(expectedPerson.getIdPerson());
  }
  
  /**
   * Test creating person with null name.
   */
  @Test(priority = 35, expectedExceptions = ConstraintViolationException.class)
  public void testNullName() {
    Person newPerson = new Person();
    newPerson.setFirstName(null);
    newPerson.setLastName(RandomString.make(50));
    newPerson.setAge(38);
    newPerson.setFavouriteColour(RandomString.make(20));
    Person expectedPerson = service.save(newPerson);
    assertNotNull(expectedPerson.getIdPerson());
  }
  
  /**
   * Test creating person with long lastame.
   */
  @Test(priority = 40, expectedExceptions = ConstraintViolationException.class)
  public void testLongLastName() {
    Person newPerson = new Person();
    newPerson.setFirstName(RandomString.make(50));
    newPerson.setLastName(RandomString.make(300));
    newPerson.setAge(38);
    newPerson.setFavouriteColour(RandomString.make(20));
    Person expectedPerson = service.save(newPerson);
    assertNotNull(expectedPerson.getIdPerson());
  }

  /**
   * Test creating person with short lastame.
   */
  @Test(priority = 43, expectedExceptions = ConstraintViolationException.class)
  public void testShortLastName() {
    Person newPerson = new Person();
    newPerson.setFirstName(RandomString.make(50));
    newPerson.setLastName("");
    newPerson.setAge(38);
    newPerson.setFavouriteColour(RandomString.make(20));
    Person expectedPerson = service.save(newPerson);
    assertNotNull(expectedPerson.getIdPerson());
  }

  /**
   * Test creating person with null lastame.
   */
  @Test(priority = 45, expectedExceptions = ConstraintViolationException.class)
  public void testNullLastName() {
    Person newPerson = new Person();
    newPerson.setFirstName(RandomString.make(50));
    newPerson.setLastName(null);
    newPerson.setAge(38);
    newPerson.setFavouriteColour("Red");
    Person expectedPerson = service.save(newPerson);
    assertNotNull(expectedPerson.getIdPerson());
  }

  /**
   * Test creating person with invalid long age.
   */
  @Test(priority = 50, expectedExceptions = ConstraintViolationException.class)
  public void testLongAge() {
    Person newPerson = new Person();
    newPerson.setFirstName(RandomString.make(50));
    newPerson.setLastName(RandomString.make(50));
    newPerson.setAge(150);
    newPerson.setFavouriteColour("Red");
    Person expectedPerson = service.save(newPerson);
    assertNotNull(expectedPerson.getIdPerson());
  }
  
  /**
   * Test creating person with invalid short age.
   */
  @Test(priority = 55, expectedExceptions = ConstraintViolationException.class)
  public void testShortAge() {
    Person newPerson = new Person();
    newPerson.setFirstName(RandomString.make(50));
    newPerson.setLastName(RandomString.make(50));
    newPerson.setAge(0);
    newPerson.setFavouriteColour("Red");
    Person expectedPerson = service.save(newPerson);
    assertNotNull(expectedPerson.getIdPerson());
  }
  
  /**
   * Test creating person with null age.
   */
  @Test(priority = 60, expectedExceptions = ConstraintViolationException.class)
  public void testNullAge() {
    Person newPerson = new Person();
    newPerson.setFirstName(RandomString.make(50));
    newPerson.setLastName(RandomString.make(50));
    newPerson.setAge(150);
    newPerson.setFavouriteColour("Red");
    Person expectedPerson = service.save(newPerson);
    assertNotNull(expectedPerson.getIdPerson());
  }

  /**
   * Test creating person with long color.
   */
  @Test(priority = 70, expectedExceptions = ConstraintViolationException.class)
  public void testLongColor() {
    Person newPerson = new Person();
    newPerson.setFirstName(RandomString.make(50));
    newPerson.setLastName(RandomString.make(50));
    newPerson.setAge(40);
    newPerson.setFavouriteColour(RandomString.make(101));
    Person expectedPerson = service.save(newPerson);
    assertNotNull(expectedPerson.getIdPerson());
  }
  
  /**
   * Test creating person with short color.
   */
  @Test(priority = 80, expectedExceptions = ConstraintViolationException.class)
  public void testShortColor() {
    Person newPerson = new Person();
    newPerson.setFirstName(RandomString.make(50));
    newPerson.setLastName(RandomString.make(50));
    newPerson.setAge(40);
    newPerson.setFavouriteColour("");
    Person expectedPerson = service.save(newPerson);
    assertNotNull(expectedPerson.getIdPerson());
  }
  
  /**
   * Test creating person with null color.
   */
  @Test(priority = 90, expectedExceptions = ConstraintViolationException.class)
  public void testNullColor() {
    Person newPerson = new Person();
    newPerson.setFirstName(RandomString.make(50));
    newPerson.setLastName(RandomString.make(50));
    newPerson.setAge(40);
    newPerson.setFavouriteColour(null);
    Person expectedPerson = service.save(newPerson);
    assertNotNull(expectedPerson.getIdPerson());
  }
  
  /*
   * @Test(expectedExceptions = ConstraintViolationException.class) public void
   * testSaveCardInvalidQuestion() { // null question Card actual = new Card(null,
   * VALID_QUESTION_OR_ANSWER); service.saveCard(actual);
   * 
   * // empty question actual = new Card("", VALID_QUESTION_OR_ANSWER); service.saveCard(actual);
   * 
   * // short question actual = new Card("12", VALID_QUESTION_OR_ANSWER); service.saveCard(actual);
   * 
   * // too long question actual = new Card(INVALID_SUPER_LONG_STRING, VALID_QUESTION_OR_ANSWER);
   * service.saveCard(actual);
   * 
   * }
   * 
   * @Test(expectedExceptions = ConstraintViolationException.class) public void
   * testSaveCardInvalidAnswer() { // null Card actual = new Card(VALID_QUESTION_OR_ANSWER, null);
   * service.saveCard(actual);
   * 
   * // empty actual = new Card(VALID_QUESTION_OR_ANSWER, ""); service.saveCard(actual);
   * 
   * // short actual = new Card(VALID_QUESTION_OR_ANSWER, "12"); service.saveCard(actual);
   * 
   * // too long actual = new Card(VALID_QUESTION_OR_ANSWER, INVALID_SUPER_LONG_STRING);
   * service.saveCard(actual);
   * 
   * }
   */


}
