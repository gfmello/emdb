package gfmello.emdb.test.functional;

import static org.junit.Assert.assertNotEquals;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import gfmello.emdb.person.Person;
import gfmello.emdb.person.PersonService;
import gfmello.emdb.person.PersonWrapper;
import java.io.IOException;
import java.util.Arrays;
import net.bytebuddy.utility.RandomString;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.testng.annotations.Test;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class LoginControllerTest extends AbstractTestNGSpringContextTests {

  private static final ObjectMapper mapper = new ObjectMapper();

  private static final String HEADER_AUTH = "Authorization";

  private static final String HEADER_TOKEN = "token";

  private static final String LOGIN_JSON = "{\"username\": \"%s\",\"password\": \"%s\"}";

  @LocalServerPort
  int randomServerPort;

  @Autowired
  private PersonService service;

  private String userToken;

  private String adminToken;

  @Autowired
  private TestRestTemplate restTemplate;

  /**
   * Test creation of usertoken.
   * 
   * @throws Exception error
   */
  @Test(priority = 1)
  public void test_ws_login_create_user_jwt_token() throws Exception {
    HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.APPLICATION_JSON);

    HttpEntity<String> entity = new HttpEntity<String>(getLoginJson("user"), headers);
    ResponseEntity<String> result = restTemplate.postForEntity("/login", entity, String.class);
    assertEquals(result.getStatusCodeValue(), HttpStatus.OK.value());
    assertNotNull(result.getHeaders().get(HEADER_TOKEN).get(0));
    assertNotEquals(result.getHeaders().get(HEADER_TOKEN).get(0).length(), 0);
    userToken = getTokenFromHeader(result.getHeaders());
  }

  /**
   * Test creation admin token.
   * 
   * @throws Exception error
   */
  @Test(priority = 1)
  public void test_ws_login_create_admin_jwt_token() throws Exception {
    HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.APPLICATION_JSON);

    HttpEntity<String> entity = new HttpEntity<String>(getLoginJson("admin"), headers);
    ResponseEntity<String> result = restTemplate.postForEntity("/login", entity, String.class);
    assertEquals(result.getStatusCodeValue(), HttpStatus.OK.value());
    assertNotNull(result.getHeaders().get(HEADER_TOKEN).get(0));
    assertNotEquals(result.getHeaders().get(HEADER_TOKEN).get(0).length(), 0);
    adminToken = getTokenFromHeader(result.getHeaders());
  }



  // ~ Test Create. Only authenticated users can.
  // ================================================================================

  @Test(priority = 100)
  public void test_ws_create_no_credential() throws JsonProcessingException {
    ResponseEntity<String> result = templateCreateWsTest(null);
    assertEquals(result.getStatusCodeValue(), HttpStatus.UNAUTHORIZED.value());
  }

  @Test(priority = 100)
  public void test_ws_create_valid_user_credential() throws JsonProcessingException {
    ResponseEntity<String> result = templateCreateWsTest(userToken);
    assertEquals(result.getStatusCodeValue(), HttpStatus.CREATED.value());
  }

  @Test(priority = 100)
  public void test_ws_create_valid_admin_credential() throws JsonProcessingException {
    ResponseEntity<String> result = templateCreateWsTest(adminToken);
    assertEquals(result.getStatusCodeValue(), HttpStatus.CREATED.value());
  }

  private ResponseEntity<String> templateCreateWsTest(String token) throws JsonProcessingException {
    HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.APPLICATION_JSON);
    if (token != null) {
      headers.set(HEADER_AUTH, token);
    }
    Person person = new Person();
    person.setAge(10);
    person.setFavouriteColour(RandomString.make(10));
    person.setFirstName(RandomString.make(10));
    person.setLastName(RandomString.make(10));
    String jsonPerson = mapper.writeValueAsString(person);
    HttpEntity<String> entity = new HttpEntity<String>(jsonPerson, headers);
    ResponseEntity<String> result =
        restTemplate.exchange("/person", HttpMethod.POST, entity, String.class);
    return result;
  }


  // ~ Test FindAll. Only authenticated users can.
  // ================================================================================

  @Test(priority = 100)
  public void test_ws_list_no_credential() {
    HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.APPLICATION_JSON);

    HttpEntity<String> entity = new HttpEntity<String>(headers);
    ResponseEntity<String> result =
        restTemplate.exchange("/person", HttpMethod.GET, entity, String.class);

    assertEquals(result.getStatusCodeValue(), HttpStatus.UNAUTHORIZED.value());
  }

  @Test(priority = 100)
  public void test_ws_list_valid_user_credential() {
    HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.APPLICATION_JSON);
    headers.set(HEADER_AUTH, userToken);

    HttpEntity<String> entity = new HttpEntity<String>(headers);
    ResponseEntity<String> result =
        restTemplate.exchange("/person", HttpMethod.GET, entity, String.class);

    assertEquals(result.getStatusCodeValue(), HttpStatus.OK.value());
  }

  @Test(priority = 100)
  public void test_ws_list_valid_admin_credential() {
    HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.APPLICATION_JSON);
    headers.set(HEADER_AUTH, adminToken);

    HttpEntity<String> entity = new HttpEntity<String>(headers);
    ResponseEntity<String> result =
        restTemplate.exchange("/person", HttpMethod.GET, entity, String.class);

    assertEquals(result.getStatusCodeValue(), HttpStatus.OK.value());
  }

  // ~ Test FindById. Only authenticated users can.
  // ================================================================================

  @Test(priority = 100)
  public void test_ws_findbyid_no_credential()
      throws JsonParseException, JsonMappingException, IOException {
    ResponseEntity<String> result = templateFindByIdWsTest(null);
    assertEquals(result.getStatusCodeValue(), HttpStatus.UNAUTHORIZED.value());
  }

  @Test(priority = 100)
  public void test_ws_findbyid_valid_user_credential()
      throws JsonParseException, JsonMappingException, IOException {
    ResponseEntity<String> result = templateFindByIdWsTest(userToken);
    assertEquals(result.getStatusCodeValue(), HttpStatus.OK.value());
  }

  @Test(priority = 100)
  public void test_ws_findbyid_valid_admin_credential()
      throws JsonParseException, JsonMappingException, IOException {
    ResponseEntity<String> result = templateFindByIdWsTest(adminToken);
    assertEquals(result.getStatusCodeValue(), HttpStatus.OK.value());
  }

  private ResponseEntity<String> templateFindByIdWsTest(String token)
      throws JsonParseException, JsonMappingException, IOException {
    HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.APPLICATION_JSON);
    if (token != null) {
      headers.set(HEADER_AUTH, token);
    }

    // find existing from db a
    Person personFromDb = service.findAll().get(0);
    HttpEntity<String> entity = new HttpEntity<String>(headers);
    ResponseEntity<String> result = restTemplate.exchange("/person/" + personFromDb.getIdPerson(),
        HttpMethod.GET, entity, String.class);

    if (HttpStatus.OK.equals(result.getStatusCode())) {
      Person personFromWs = mapper.readValue(result.getBody(), Person.class);
      assertEquals(personFromDb.toString(), personFromWs.toString());
    }

    return result;
  }

  // ~ Test Create Batch. Only authenticated users can.
  // ================================================================================

  @Test(priority = 100)
  public void test_ws_createall_no_credential() throws JsonProcessingException {
    ResponseEntity<String> result = templateCreateAllWsTest(null);
    assertEquals(result.getStatusCodeValue(), HttpStatus.UNAUTHORIZED.value());
  }

  @Test(priority = 100)
  public void test_ws_createall_valid_user_credential() throws JsonProcessingException {
    ResponseEntity<String> result = templateCreateAllWsTest(userToken);
    assertEquals(result.getStatusCodeValue(), HttpStatus.OK.value());
  }

  @Test(priority = 100)
  public void test_ws_createall_valid_admin_credential() throws JsonProcessingException {
    ResponseEntity<String> result = templateCreateAllWsTest(adminToken);
    assertEquals(result.getStatusCodeValue(), HttpStatus.OK.value());
  }

  private ResponseEntity<String> templateCreateAllWsTest(String token)
      throws JsonProcessingException {
    HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.APPLICATION_JSON);
    if (token != null) {
      headers.set(HEADER_AUTH, token);
    }

    Person person = new Person();
    person.setAge(10);
    person.setFavouriteColour(RandomString.make(10));
    person.setFirstName(RandomString.make(10));
    person.setLastName(RandomString.make(10));

    Person person2 = new Person();
    person2.setAge(10);
    person2.setFavouriteColour(RandomString.make(10));
    person2.setFirstName(RandomString.make(10));
    person2.setLastName(RandomString.make(10));

    PersonWrapper wrapper = new PersonWrapper();
    wrapper.setPerson(Arrays.asList(person, person2));

    String wrapperJson = mapper.writeValueAsString(wrapper);

    HttpEntity<String> entity = new HttpEntity<String>(wrapperJson, headers);
    ResponseEntity<String> result =
        restTemplate.exchange("/person/batch", HttpMethod.POST, entity, String.class);
    return result;
  }


  // ~ Test Update. Only admin users can.
  // ================================================================================

  @Test(priority = 200)
  public void test_ws_update_valid_user_credential() throws JsonProcessingException {
    ResponseEntity<String> result = templateUpdateWsTest(userToken);
    assertEquals(result.getStatusCodeValue(), HttpStatus.FORBIDDEN.value());
  }

  @Test(priority = 200)
  public void test_ws_update_valid_admin_credential() throws JsonProcessingException {
    ResponseEntity<String> result = templateUpdateWsTest(adminToken);
    assertEquals(result.getStatusCodeValue(), HttpStatus.OK.value());
  }

  @Test(priority = 200)
  public void test_ws_update_no_credential() throws JsonProcessingException {
    ResponseEntity<String> result = templateUpdateWsTest(null);
    assertEquals(result.getStatusCodeValue(), HttpStatus.UNAUTHORIZED.value());
  }

  private ResponseEntity<String> templateUpdateWsTest(String token) throws JsonProcessingException {
    HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.APPLICATION_JSON);
    if (token != null) {
      headers.set(HEADER_AUTH, token);
    }
    // find existing from db and update a field
    Person personFromDb = service.findAll().get(0);
    personFromDb.setFirstName(RandomString.make(11));
    // converst to json
    String json = mapper.writeValueAsString(personFromDb);

    HttpEntity<String> entity = new HttpEntity<String>(json, headers);
    ResponseEntity<String> result = restTemplate.exchange("/person/" + personFromDb.getIdPerson(),
        HttpMethod.PUT, entity, String.class);
    return result;
  }

  // ~ Test Patch. Only admin users can.
  // ================================================================================

  @Test(priority = 300, expectedExceptions = HttpClientErrorException.class)
  public void test_ws_patch_no_credential() throws IOException {
    ResponseEntity<String> result = templatePatchWsTest(null);
    assertEquals(result.getStatusCodeValue(), HttpStatus.UNAUTHORIZED.value());
  }

  @Test(priority = 300, expectedExceptions = HttpClientErrorException.class)
  public void test_ws_patch_valid_user_credential() throws IOException {
    ResponseEntity<String> result = templatePatchWsTest(userToken);
    assertEquals(result.getStatusCodeValue(), HttpStatus.FORBIDDEN.value());
  }

  @Test(priority = 300)
  public void test_ws_patch_valid_admin_credential() throws IOException {
    ResponseEntity<String> result = templatePatchWsTest(adminToken);
    assertEquals(result.getStatusCodeValue(), HttpStatus.OK.value());
  }

  private ResponseEntity<String> templatePatchWsTest(String token) throws IOException {
    HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.APPLICATION_JSON);
    if (token != null) {
      headers.set(HEADER_AUTH, token);
    }

    RestTemplate customRestTemplate = new RestTemplate();
    HttpComponentsClientHttpRequestFactory requestFactory =
        new HttpComponentsClientHttpRequestFactory();
    requestFactory.setConnectTimeout(1000);
    requestFactory.setReadTimeout(1000);

    customRestTemplate.setRequestFactory(requestFactory);

    // find existing from db and update a field
    Person personFromDb = service.findAll().get(0);
    personFromDb.setFirstName(RandomString.make(11));
    // converst to json
    String json = mapper.writeValueAsString(personFromDb);

    HttpEntity<String> entity = new HttpEntity<String>(json, headers);
    ResponseEntity<String> result = customRestTemplate.exchange(
        "http://localhost:" + randomServerPort + "/person/" + personFromDb.getIdPerson(),
        HttpMethod.PATCH, entity, String.class);

    if (HttpStatus.OK.equals(result.getStatusCode())) {
      Person personFromWs = mapper.readValue(result.getBody(), Person.class);
      assertEquals(personFromDb.getIdPerson(), personFromWs.getIdPerson());
    }

    return result;
  }

  // ~ Test Delete. Only admin users can.
  // ================================================================================

  @Test(priority = 300)
  public void test_ws_delete_no_credential() throws JsonProcessingException {
    ResponseEntity<String> result = templateDeleteWsTest(null);
    assertEquals(result.getStatusCodeValue(), HttpStatus.UNAUTHORIZED.value());

  }

  @Test(priority = 300)
  public void test_ws_delete_valid_usero_credential() throws JsonProcessingException {
    ResponseEntity<String> result = templateDeleteWsTest(userToken);
    assertEquals(result.getStatusCodeValue(), HttpStatus.FORBIDDEN.value());
  }

  @Test(priority = 300)
  public void test_ws_delete_valid_admin_credential() throws JsonProcessingException {
    ResponseEntity<String> result = templateDeleteWsTest(adminToken);
    assertEquals(result.getStatusCodeValue(), HttpStatus.OK.value());
  }

  private ResponseEntity<String> templateDeleteWsTest(String token) {
    HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.APPLICATION_JSON);
    if (token != null) {
      headers.set(HEADER_AUTH, token);
    }
    // person with only id and last name filled
    HttpEntity<String> entity = new HttpEntity<String>(null, headers);
    Long idPersonToDelete = service.findAll().get(0).getIdPerson();
    ResponseEntity<String> result = restTemplate.exchange("/person/" + idPersonToDelete,
        HttpMethod.DELETE, entity, String.class);
    return result;
  }

  private String getTokenFromHeader(HttpHeaders headers) throws Exception {
    try {
      String token = headers.get(HEADER_TOKEN).get(0);
      assertNotNull(token);
      return token;
    } catch (Exception e) {
      throw new Exception("Could not get token variable from header");
    }
  }

  private String getLoginJson(String username) {
    return String.format(LOGIN_JSON, username, username);
  }

}
