package gfmello.emdb.exception;

import io.jsonwebtoken.JwtException;
import java.util.stream.Collectors;
import javax.persistence.EntityNotFoundException;
import org.hibernate.PropertyValueException;
import org.hibernate.exception.ConstraintViolationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

/**
 * Rest webservice exception handler.
 * 
 * @author gui_u
 *
 */
@RestControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

  private static final Logger logger = LoggerFactory.getLogger(RestExceptionHandler.class);

  /**
   * Capture exception details into ws rest response.
   * @param ex Exception
   * @param request Request
   * @return
   */
  @ExceptionHandler({ ConstraintViolationException.class })
  public ResponseEntity<Object> violatedConstraint(ConstraintViolationException ex,
      WebRequest request) {
    logger.debug(ex.getMessage(), ex);
    return handleExceptionInternal(ex,
        ErrorDetail.builder().addDetalhe("Violated Constraint: " + ex.getConstraintName())
            .addErro(ex.getMessage()).addStatus(HttpStatus.CONFLICT)
            .addHttpMethod(getHttpMethod(request)).addPath(getPath(request)).build(),
        new HttpHeaders(), HttpStatus.CONFLICT, request);
  }

  /**
   * Capture exception details into ws rest response.
   * @param ex Exception
   * @param request Request
   * @return
   */
  @ExceptionHandler({ javax.validation.ConstraintViolationException.class })
  public ResponseEntity<Object> beanValidationException(
      javax.validation.ConstraintViolationException ex, WebRequest request) {
    logger.debug(ex.getMessage(), ex);

    String errorMessage = ex.getConstraintViolations().stream().map((p) -> p.getMessage())
        .collect(Collectors.joining(", "));

    return handleExceptionInternal(ex,
        ErrorDetail.builder().addDetalhe("Violated Bean Validation Constraints: ")
            .addErro(errorMessage).addStatus(HttpStatus.UNPROCESSABLE_ENTITY)
            .addHttpMethod(getHttpMethod(request)).addPath(getPath(request)).build(),
        new HttpHeaders(), HttpStatus.UNPROCESSABLE_ENTITY, request);
  }

  /**
   * Capture exception details into ws rest response.
   * @param ex Exception
   * @param request Request
   * @return
   */
  @ExceptionHandler({ PropertyValueException.class })
  public ResponseEntity<Object> emptyProperty(PropertyValueException ex, WebRequest request) {
    logger.debug(ex.getMessage(), ex);
    return handleExceptionInternal(ex,
        ErrorDetail.builder()
            .addDetalhe("The attribute " + ex.getPropertyName() + " cannot be null.")
            .addErro(ex.getMessage()).addStatus(HttpStatus.NOT_FOUND)
            .addHttpMethod(getHttpMethod(request)).addPath(getPath(request)).build(),
        new HttpHeaders(), HttpStatus.NOT_FOUND, request);
  }

  /**
   * Capture exception details into ws rest response.
   * @param ex Exception
   * @param request Request
   * @return
   */
  @ExceptionHandler({ EmptyResultDataAccessException.class })
  public ResponseEntity<Object> emptyResult(EmptyResultDataAccessException ex, WebRequest request) {
    logger.debug(ex.getMessage(), ex);
    return handleExceptionInternal(ex,
        ErrorDetail.builder().addDetalhe("Empty result from Database").addErro(ex.getMessage())
            .addStatus(HttpStatus.NOT_FOUND).addHttpMethod(getHttpMethod(request))
            .addPath(getPath(request)).build(),
        new HttpHeaders(), HttpStatus.NOT_FOUND, request);
  }
  
  /**
   * Capture exception details into ws rest response.
   * @param ex Exception
   * @param request Request
   * @return
   */
  @ExceptionHandler({ EntityNotFoundException.class })
  public ResponseEntity<Object> notfound(EntityNotFoundException ex, WebRequest request) {
    logger.debug(ex.getMessage(), ex);
    return handleExceptionInternal(ex,
        ErrorDetail.builder().addDetalhe("Resource Not Found on Database").addErro(ex.getMessage())
            .addStatus(HttpStatus.NOT_FOUND).addHttpMethod(getHttpMethod(request))
            .addPath(getPath(request)).build(),
        new HttpHeaders(), HttpStatus.NOT_FOUND, request);
  }

  /**
   * Capture exception details into ws rest response.
   * @param ex Exception
   * @param request Request
   * @return
   */
  @ExceptionHandler({ JwtException.class })
  public ResponseEntity<Object> serverException(JwtException ex, WebRequest request) {
    logger.debug(ex.getMessage(), ex);
    return handleExceptionInternal(ex,
        ErrorDetail.builder().addDetalhe("The JWT Token is invalid.").addErro(ex.getMessage())
            .addStatus(HttpStatus.UNAUTHORIZED).addHttpMethod(getHttpMethod(request))
            .addPath(getPath(request)).build(),
        new HttpHeaders(), HttpStatus.UNAUTHORIZED, request);
  }
  
  /**
   * Capture exception details into ws rest response.
   * @param ex Exception
   * @param request Request
   * @return
   */
  @ExceptionHandler({ AccessDeniedException.class })
  public ResponseEntity<Object> serverException(AccessDeniedException ex, WebRequest request) {
    logger.debug(ex.getMessage(), ex);
    return handleExceptionInternal(ex,
        ErrorDetail.builder().addDetalhe("Access forbidden.").addErro(ex.getMessage())
            .addStatus(HttpStatus.FORBIDDEN).addHttpMethod(getHttpMethod(request))
            .addPath(getPath(request)).build(),
        new HttpHeaders(), HttpStatus.FORBIDDEN, request);
  }
  
  /**
   * Capture exception details into ws rest response.
   * @param ex Exception
   * @param request Request
   * @return
   */
  @ExceptionHandler({ RuntimeException.class })
  public ResponseEntity<Object> serverException(RuntimeException ex, WebRequest request) {
    logger.debug(ex.getMessage(), ex);
    return handleExceptionInternal(ex,
        ErrorDetail.builder().addDetalhe("An Error Occurred").addErro(ex.getMessage())
            .addStatus(HttpStatus.INTERNAL_SERVER_ERROR).addHttpMethod(getHttpMethod(request))
            .addPath(getPath(request)).build(),
        new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR, request);
  }

  private String getPath(WebRequest request) {
    return ((ServletWebRequest) request).getRequest().getRequestURI();
  }

  private String getHttpMethod(WebRequest request) {
    return ((ServletWebRequest) request).getRequest().getMethod();
  }

}
