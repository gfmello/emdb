package gfmello.emdb.exception;

import org.springframework.security.core.AuthenticationException;

public class JwtTokenMissingException extends AuthenticationException {

  private static final long serialVersionUID = -4739677845060758347L;

  public JwtTokenMissingException(String message, Throwable cause) {
    super(message, cause);
  }

  public JwtTokenMissingException(String message) {
    super(message);
  }

}
