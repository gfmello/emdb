package gfmello.emdb.exception;

import java.io.Serializable;
import org.springframework.http.HttpStatus;

/**
 * Wrapper to return error messages for the client webservice.
 * 
 * @author gui_u
 *
 */
public class ErrorDetail implements Serializable {

  private static final long serialVersionUID = 1L;
  private Integer statusCode;
  private String statusMessage;
  private String httpMethod;
  private String error;
  private String detail;
  private String path;

  public static Builder builder() {
    return new Builder();
  }

  public static class Builder {

    private ErrorDetail erro;

    Builder() {
      this.erro = new ErrorDetail();
    }

    /**
     * Add http status errors info to the detail.
     * @param status httpstatus
     * @return
     */
    public Builder addStatus(HttpStatus status) {
      this.erro.statusCode = status.value();
      this.erro.statusMessage = status.getReasonPhrase();
      return this;
    }

    public Builder addHttpMethod(String method) {
      this.erro.httpMethod = method;
      return this;
    }

    public Builder addErro(String error) {
      this.erro.error = error;
      return this;
    }

    public Builder addDetalhe(String detail) {
      this.erro.detail = detail;
      return this;
    }

    public Builder addPath(String path) {
      this.erro.path = path;
      return this;
    }

    public ErrorDetail build() {
      return this.erro;
    }
  }

  public Integer getStatusCode() {
    return statusCode;
  }

  public void setStatusCode(Integer statusCode) {
    this.statusCode = statusCode;
  }

  public String getStatusMessage() {
    return statusMessage;
  }

  public void setStatusMessage(String statusMessage) {
    this.statusMessage = statusMessage;
  }

  public String getHttpMethod() {
    return httpMethod;
  }

  public void setHttpMethod(String httpMethod) {
    this.httpMethod = httpMethod;
  }

  public String getError() {
    return error;
  }

  public void setError(String error) {
    this.error = error;
  }

  public String getDetail() {
    return detail;
  }

  public void setDetail(String detail) {
    this.detail = detail;
  }

  public String getPath() {
    return path;
  }

  public void setPath(String path) {
    this.path = path;
  }


}
