package gfmello.emdb.person;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import java.time.Duration;
import java.time.Instant;
import java.util.List;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/person")
@CrossOrigin
public class PersonController {

  private static final Logger logger = LoggerFactory.getLogger(PersonController.class);

  private PersonService service;

  // Object to json converter
  private final ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();

  @Autowired
  public PersonController(PersonService service) {
    super();
    this.service = service;
  }

  /**
   * Create.
   * 
   * @param personToSave person
   * @return
   */
  @PreAuthorize("isAuthenticated()")
  @RequestMapping(consumes = MediaType.APPLICATION_JSON_VALUE,
      produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
  public ResponseEntity<Person> addPerson(@RequestBody Person personToSave) {
    personToSave = service.save(personToSave);
    ResponseEntity<Person> response = new ResponseEntity<Person>(personToSave, HttpStatus.CREATED);
    logger.debug("Returning Person Create Webservice: {}", response);
    return response;
  }


  /**
   * Create Multiple.
   * 
   * @param wrapper list of persons
   */
  @Async
  @PreAuthorize("isAuthenticated()")
  @RequestMapping(value = "/batch", consumes = MediaType.APPLICATION_JSON_VALUE,
      produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
  public void addPersonBatch(@RequestBody PersonWrapper wrapper) {
    Instant start = Instant.now();
    service.saveAll(wrapper.getPerson());
    Instant end = Instant.now();
    logger.debug("addPersonBatch lasted: {}", Duration.between(start, end));
  }


  /**
   * Update.
   * 
   * @param idPerson idPerson to update
   * @param personToUpdate person data
   * @return updated person
   */
  @PreAuthorize("hasRole('ADMIN')")
  @RequestMapping(value = "/{idPerson}", consumes = MediaType.APPLICATION_JSON_VALUE,
      produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.PUT)
  public ResponseEntity<Person> updatePerson(@PathVariable Long idPerson,
      @RequestBody Person personToUpdate) {
    personToUpdate.setIdPerson(idPerson);
    personToUpdate = service.update(personToUpdate);
    ResponseEntity<Person> response = new ResponseEntity<Person>(personToUpdate, HttpStatus.OK);
    logger.debug("Returning Update Person Webservice: {}", response);
    return response;
  }

  /**
   * Patch.
   * 
   * @param idPerson idPerson to update
   * @param personToUpdate person with some data
   * @return updated person
   */
  @PreAuthorize("hasRole('ADMIN')")
  @RequestMapping(value = "/{idPerson}", consumes = MediaType.APPLICATION_JSON_VALUE,
      produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.PATCH)
  public ResponseEntity<Person> patchPerson(@PathVariable Long idPerson,
      @RequestBody Person personToUpdate) {
    personToUpdate.setIdPerson(idPerson);
    personToUpdate = service.patchUpdate(personToUpdate);
    ResponseEntity<Person> response = new ResponseEntity<Person>(personToUpdate, HttpStatus.OK);
    logger.debug("Returning Patch Update Person Webservice: {}", response);
    return response;
  }

  /**
   * Delete.
   * 
   * @param idPerson id person to delete
   * @return
   */
  @PreAuthorize("hasRole('ROLE_ADMIN')")
  @RequestMapping(value = "/{idPerson}", produces = MediaType.APPLICATION_JSON_VALUE,
      method = RequestMethod.DELETE)
  public ResponseEntity<Void> deletePerson(@PathVariable Long idPerson) {
    service.deleteById(idPerson);
    ResponseEntity<Void> response = new ResponseEntity<Void>(HttpStatus.OK);
    logger.debug("Deleted Person Update Webservice: {}", idPerson);
    return response;
  }

  /**
   * Find All.
   * 
   * @return List of person in json format
   * @throws JsonProcessingException Error if List cannot be converted to json.
   */
  @PreAuthorize("isAuthenticated()")
  @RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.GET)
  public ResponseEntity<String> findAll() throws JsonProcessingException {
    List<Person> allPersons = service.findAllSortedByName();
    String jsonPersonList = ow.writeValueAsString(allPersons); // object to json convertion
    ResponseEntity<String> response = new ResponseEntity<String>(jsonPersonList, HttpStatus.OK);
    logger.debug("list person: {}", jsonPersonList);
    return response;
  }


  /**
   * Find by Id.
   * 
   * @param idPerson person id to find.
   * @return Found person.
   * @throws JsonProcessingException json convertion error.
   */
  @PreAuthorize("isAuthenticated()")
  @RequestMapping(value = "/{idPerson}", produces = MediaType.APPLICATION_JSON_VALUE,
      method = RequestMethod.GET)
  public ResponseEntity<Person> findById(@PathVariable Long idPerson)
      throws JsonProcessingException {
    Optional<Person> optPerson = service.findById(idPerson);
    ResponseEntity<Person> response = new ResponseEntity<Person>(optPerson.get(), HttpStatus.OK);
    logger.debug("find person by id: {}", response);
    return response;
  }
}

