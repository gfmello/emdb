package gfmello.emdb.person;

import java.util.List;

/**
 * Wrapper to receive a list of persons in the webservice controller.
 * @author gui_u
 *
 */
public class PersonWrapper {
  
  private List<Person> person;

  public List<Person> getPerson() {
    return person;
  }

  public void setPerson(List<Person> person) {
    this.person = person;
  }

}
