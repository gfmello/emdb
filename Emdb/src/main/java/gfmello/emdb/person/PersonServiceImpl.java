package gfmello.emdb.person;

import gfmello.emdb.common.BeanHelper;
import java.util.List;
import java.util.Optional;
import javax.persistence.EntityNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(timeout = 60)
public class PersonServiceImpl implements PersonService {

  private static final Logger logger = LoggerFactory.getLogger(PersonServiceImpl.class);

  private PersonDao dao;

  @Autowired
  public PersonServiceImpl(PersonDao dao) {
    super();
    this.dao = dao;
  }

  @Override
  public Person save(Person newPerson) {
    logger.debug("save({})", newPerson);
    BeanHelper.validateModel(newPerson);
    return dao.save(newPerson);
  }

  @Override
  public List<Person> findAll() {
    logger.trace("findAll()");
    return dao.findAll();
  }

  @Override
  public List<Person> findAllSortedByName() {
    logger.trace("findAllSortedByName()");
    return dao.findByOrderByFirstNameAscLastNameAsc();
  }

  @Override
  public void deleteById(Long idPerson) {
    logger.debug("deleteById({})", idPerson);
    dao.deleteById(idPerson);
  }

  @Override
  public Person update(Person personToUpdate) {
    logger.debug("update({})", personToUpdate);
    BeanHelper.validateModel(personToUpdate);
    if (dao.existsById(personToUpdate.getIdPerson())) {
      return dao.save(personToUpdate);
    }
    throw new EntityNotFoundException(String.format(
        "The person %s you are trying to update does not exist in the database.", personToUpdate));
  }

  @Override
  public Person patchUpdate(Person personPatch) {
    logger.debug("patchUpdate({})", personPatch);
    Optional<Person> dbPersonOpt = dao.findById(personPatch.getIdPerson());
    if (dbPersonOpt.isPresent()) {
      Person dbPerson = dbPersonOpt.get();
      BeanUtils.copyProperties(personPatch, dbPerson,
          BeanHelper.getNullPropertiesNames(personPatch));
      return dao.save(dbPerson);
    }
    throw new EntityNotFoundException(String.format(
        "The person %s you are trying to patch  does not exist in the database.", personPatch));
  }

  @Override
  public Optional<Person> findById(Long idPerson) {
    return dao.findById(idPerson);
  }

  @Override
  public Iterable<Person> saveAll(Iterable<Person> personList) {
    return dao.saveAll(personList);
  }

}
