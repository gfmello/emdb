package gfmello.emdb.person;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Dao interface with Spring Data JPA implementation for data access.
 * @author gui_u
 *
 */
@Repository
public interface PersonDao extends JpaRepository<Person, Long> {
  
  
  /**
   * /**
   * Find all persons in the database, ordered by first and last names.
   * @return
   */
  List<Person> findByOrderByFirstNameAscLastNameAsc();

}
