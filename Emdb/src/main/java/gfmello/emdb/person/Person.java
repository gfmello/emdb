package gfmello.emdb.person;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "PERSON",
    uniqueConstraints = @UniqueConstraint(columnNames = { "firstName", "lastName", "age" }))
public class Person implements Serializable {

  private static final long serialVersionUID = -3985419625034991111L;

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "ID_PERSON")
  private Long idPerson;

  @JsonProperty("first_name")
  @NotNull
  @Size(min = 1, max = 255)
  @Column(name = "FIRSTNAME")
  private String firstName;

  @JsonProperty("last_name")
  @NotNull
  @Size(min = 1, max = 255)
  @Column(name = "LASTNAME")
  private String lastName;

  @NotNull
  @Min(1)
  @Max(130)
  @Column(name = "AGE")
  private Integer age;

  @JsonProperty("favourite_colour")
  @NotNull
  @Size(min = 1, max = 100)
  @Column(name = "FAVOURITECOLOUR")
  private String favouriteColour;

  public Long getIdPerson() {
    return idPerson;
  }

  public void setIdPerson(Long idPerson) {
    this.idPerson = idPerson;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public Integer getAge() {
    return age;
  }

  public void setAge(Integer age) {
    this.age = age;
  }

  public String getFavouriteColour() {
    return favouriteColour;
  }

  public void setFavouriteColour(String favouriteColour) {
    this.favouriteColour = favouriteColour;
  }

  @Override
  public String toString() {
    return "Person [idPerson=" + idPerson + ", firstName=" + firstName + ", lastName=" + lastName
        + ", age=" + age + ", favouriteColour=" + favouriteColour + "]";
  }

}
