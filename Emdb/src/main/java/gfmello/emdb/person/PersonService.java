package gfmello.emdb.person;

import java.util.List;
import java.util.Optional;

public interface PersonService {

  /**
   * Saves/updates a new person in the database.
   * 
   * @param newPerson person to create
   * @return
   */
  Person save(Person newPerson);

  /**
   * Returns all persons from database.
   * 
   * @return
   */
  List<Person> findAll();

  /**
   * Returns all persons from database, sorted by name.
   * 
   * @return
   */
  List<Person> findAllSortedByName();

  /**
   * Delete a person by id.
   * 
   * @param idPerson person id
   */
  void deleteById(Long idPerson);

  /**
   * Updates a person in the database. The id is mandatory.
   * 
   * @param personToUpdate person with id
   * @return
   */
  Person update(Person personToUpdate);

  /**
   * Finds a person by the Id.
   * 
   * @param idExistingPerson person id
   * @return
   */
  Optional<Person> findById(Long idExistingPerson);

  /**
   * Insert meny persons at once.
   * 
   * @param personList list of persons
   * @return
   */
  Iterable<Person> saveAll(Iterable<Person> personList);

  /**
   * Updates the non null fields of person.
   * 
   * @param personToUpdate person with non null fields to update
   * @return
   */
  Person patchUpdate(Person personToUpdate);

}
