package gfmello.emdb.common;

import java.beans.FeatureDescriptor;
import java.util.Set;
import java.util.stream.Stream;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validation;
import javax.validation.Validator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;

/**
 * Bean validation of entity elements.
 * 
 * @author gui_u
 *
 */
public final class BeanHelper {

  private static final Logger logger = LoggerFactory.getLogger(BeanHelper.class);

  private static final Validator validator =
      Validation.buildDefaultValidatorFactory().getValidator();
  
  /**
   * Bean validation of model element.
   * 
   * @param model entity bean to be validated
   * @throws ConstraintViolationException Error on bean fields
   */
  public static <T> void validateModel(T model) {
    logger.trace("Method validateModel({}) started", model);

    Set<ConstraintViolation<T>> setOfViolations = validator.validate(model);

    if (!setOfViolations.isEmpty()) {
      String errorMessage =
          String.format("The entity %s is not valid: %s ", model, setOfViolations);
      logger.debug(errorMessage);
      throw new ConstraintViolationException(errorMessage, setOfViolations);
    }
  }

  /**
   * Return the names of properties that are null.
   * @param objToInspect object to be checkd
   * @return null property names
   */
  public static String[] getNullPropertiesNames(Object objToInspect) {
    final BeanWrapper wrappedSource = new BeanWrapperImpl(objToInspect);
    return Stream.of(wrappedSource.getPropertyDescriptors()).map(FeatureDescriptor::getName)
        .filter(propertyName -> wrappedSource.getPropertyValue(propertyName) == null)
        .toArray(String[]::new);
  }

}
