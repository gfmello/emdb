package gfmello.emdb.security;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.TypeAdapter;
import com.google.gson.TypeAdapterFactory;
import com.google.gson.reflect.TypeToken;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import java.io.IOException;
import java.util.Date;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;

public class JwtUtility {

  private static final Logger logger = LoggerFactory.getLogger(JwtUtility.class);

  // TODO shoud be stored in a configuration file
  private static final String secretKey =
      "1sTOH8pgEZEHsQoJebSwwwqLGbsAwvT3JTvzanLHAqr4iI6V0spKOLFv5BJN";

  private static final Gson gson = (new GsonBuilder()).registerTypeAdapterFactory(
      new JwtUtility.GrantedAuthorityAdapterFactory(SimpleGrantedAuthority.class)).create();

  /**
   * Generates a JWT token for the user, valid for 1 hour.
   * 
   * @param user user
   * @return jwt token
   * @throws JsonProcessingException error
   */
  public static String generateToken(User user) throws JsonProcessingException {
    final Long hourNow = 1000L * 60L * 60L;
    // serialize user to store in the token
    String userJson = gson.toJson(user);
    Date dateNow = new Date();
    String token = Jwts.builder().claim("usr", userJson).setIssuer("gfmello.emdb")
        .setSubject(user.getUsername()).setExpiration(new Date(dateNow.getTime() + hourNow))
        .signWith(SignatureAlgorithm.HS256, secretKey).compact();
    logger.debug("Jwts Token [{}] created User [{}]", token, user.getUsername());
    return token;
  }

  /**
   * Retrieve user from token.
   * 
   * @param token Jwt token
   * @return userdetails from token
   * @throws JsonParseException error
   * @throws JsonMappingException error
   * @throws IOException error
   */
  public static UserDetails parseToken(String token)
      throws JsonParseException, JsonMappingException, IOException {
    String userJson = Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token).getBody()
        .get("usr", String.class);
    logger.debug("User json returned from token: {}", userJson);
    UserDetails userDetails = gson.fromJson(userJson, User.class);
    logger.debug("Jwts User [{}] retrieved from token [{}]", userDetails.getUsername(), token);
    return userDetails;
  }

  /**
   * Class to instruct gson how to instantiate the interface GrantedAuthority.
   * 
   * @author gui_u
   *
   */
  public static class GrantedAuthorityAdapterFactory implements TypeAdapterFactory {
    private final Class<? extends GrantedAuthority> implementationClass;

    public GrantedAuthorityAdapterFactory(Class<? extends GrantedAuthority> implementationClass) {
      this.implementationClass = implementationClass;
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T> TypeAdapter<T> create(Gson gson, TypeToken<T> type) {
      if (!GrantedAuthority.class.equals(type.getRawType())) {
        return null;
      }
      return (TypeAdapter<T>) gson.getAdapter(implementationClass);
    }
  }

}


