package gfmello.emdb.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.AnonymousAuthenticationFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.web.util.matcher.NegatedRequestMatcher;
import org.springframework.security.web.util.matcher.OrRequestMatcher;
import org.springframework.security.web.util.matcher.RequestMatcher;
import org.springframework.web.filter.GenericFilterBean;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SpringSecurityConfiguration extends WebSecurityConfigurerAdapter {

  private static final RequestMatcher PUBLIC_URLS = new OrRequestMatcher(
      //login url
      new AntPathRequestMatcher("/login"), 
      //health check
      new AntPathRequestMatcher("/actuator/*"),
      //swagger docs
      new AntPathRequestMatcher("/v2/api-docs"),
      new AntPathRequestMatcher("/swagger-resources/**"),
      new AntPathRequestMatcher("/swagger-ui.html**"),
      new AntPathRequestMatcher("/webjars/**"),
      new AntPathRequestMatcher("favicon.ico"));
  private static final RequestMatcher PROTECTED_URLS = new NegatedRequestMatcher(PUBLIC_URLS);

  @Autowired
  private UserDetailsService userDetailsService;

  @Override
  protected void configure(HttpSecurity http) throws Exception {
    http
        // .authenticationProvider(provider)
        .addFilterBefore(getJwtAuthenticationFilter(), AnonymousAuthenticationFilter.class)
        .authorizeRequests().requestMatchers(PUBLIC_URLS).permitAll()
        .requestMatchers(PROTECTED_URLS).authenticated().and().csrf().disable().formLogin()
        .disable().httpBasic().disable().logout().disable();
  }

  /**
   * Configure AuthenticationManager.
   */
  @Override
  protected void configure(AuthenticationManagerBuilder auth) throws Exception {
    auth.authenticationProvider(authenticationProvider());
  }

  /**
   * Create authentication filter.
   * 
   * @return filter
   * @throws Exception error
   */
  @Bean
  public FilterAuthenticationJwt getJwtAuthenticationFilter() throws Exception {
    FilterAuthenticationJwt filter = new FilterAuthenticationJwt(PROTECTED_URLS);
    filter.setAuthenticationManager(authenticationManagerBean());
    filter.setAuthenticationSuccessHandler(new JwtAuthenticationSuccessHandler());
    return filter;
  }

  /**
   * Disable Spring boot automatic filter registration.
   */
  @Bean
  FilterRegistrationBean<GenericFilterBean> disableAutoRegistration(
      final FilterAuthenticationJwt filter) {
    final FilterRegistrationBean<GenericFilterBean> registration =
        new FilterRegistrationBean<GenericFilterBean>(filter);
    registration.setEnabled(false);
    return registration;
  }

  /**
   * Configure authenticationProvider.
   * 
   * @return
   */
  @Bean
  public AuthenticationProvider authenticationProvider() {
    DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
    authProvider.setUserDetailsService(userDetailsService);
    authProvider.setPasswordEncoder(encoder());
    return authProvider;
  }

  /**
   * Return authenticationmanager.
   */
  @Bean({ "authenticationManager" })
  @Override
  public AuthenticationManager authenticationManagerBean() throws Exception {
    return super.authenticationManagerBean();
  }

  @Bean
  public PasswordEncoder encoder() {
    return new BCryptPasswordEncoder(11);
  }

}
