package gfmello.emdb.security;

import java.util.Arrays;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class SecurityServiceImpl implements UserDetailsService {

  private static final Logger logger = LoggerFactory.getLogger(SecurityServiceImpl.class);
  
  @Autowired
  private PasswordEncoder passwordEncoder;
  
  /**
   * TODO Example implementation.
   * Must use a database
   */
  @Override
  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
    logger.debug("Starting loadUserByUsername({})", username);
    
    if (username == null) {
      logger.trace("username is null");
      throw new UsernameNotFoundException(username);
    }
    
    //TODO Hardcoded. Should come from database.
    GrantedAuthority authority = null;
    if (username.equals("admin")) {
      authority = new SimpleGrantedAuthority("ROLE_ADMIN");
    } else {
      authority = new SimpleGrantedAuthority("ROLE_USER");
    }
    
    //TODO password would be retrieved encoded from database
    String password = (passwordEncoder.encode(username));
    
    //TODO UserDto from database could be wrapped in a class what implements user details
    User user = new User(username, password, Arrays.asList(authority));
    logger.debug("Finishing loadUserByUsername(): {}", user);
    return user;
  }

}
