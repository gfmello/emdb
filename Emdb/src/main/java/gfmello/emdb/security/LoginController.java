package gfmello.emdb.security;

import com.fasterxml.jackson.core.JsonProcessingException;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LoginController {

  private static final Logger logger = LoggerFactory.getLogger(LoginController.class);

  @Autowired
  @Qualifier("authenticationManager")
  private AuthenticationManager auth;

  /**
   * Login controller to retrieve the JWT token.
   * 
   * @param login with username and pwd
   * @param response token
   * @return token jwt
   * @throws JsonProcessingException error
   */
  @PreAuthorize("permitAll()")
  @RequestMapping(value = "/login", produces = MediaType.APPLICATION_JSON_VALUE,
      method = RequestMethod.POST)
  public ResponseEntity<UserDetails> login(@RequestBody LoginDto login,
      HttpServletResponse response) throws JsonProcessingException {
    logger.trace("Login WebService Invoked: {}", login);
    Authentication credentials =
        new UsernamePasswordAuthenticationToken(login.getUsername(), login.getPassword());
    User user = (User) auth.authenticate(credentials).getPrincipal();
    response.setHeader("Token", JwtUtility.generateToken(user));
    ResponseEntity<UserDetails> httpResponse = new ResponseEntity<UserDetails>(user, HttpStatus.OK);
    return httpResponse;
  }

}
