package gfmello.emdb.security;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

/**
 * This is invoked when user tries to access a secured REST resource without supplying any
 * credentials We should just send a 401 Unauthorized response because there is no 'login page' to
 * redirect to.
 * 
 * @author gui_u
 *
 */
@Component
public class ControllersAuthenticationEntryPoint implements AuthenticationEntryPoint {

  private static final Logger logger =
      LoggerFactory.getLogger(ControllersAuthenticationEntryPoint.class);

  @Override
  public void commence(HttpServletRequest req, HttpServletResponse res,
      AuthenticationException arg2) throws IOException, ServletException {
    logger.trace("ControllersAuthenticationEntryPoint.commence() Unauthorized error.");
    res.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Unauthorized");
  }

}


