package gfmello.emdb.security;

import gfmello.emdb.exception.JwtTokenMissingException;
import java.io.IOException;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.util.matcher.RequestMatcher;

public class FilterAuthenticationJwt extends AbstractAuthenticationProcessingFilter {

  private static final Logger logger = LoggerFactory.getLogger(FilterAuthenticationJwt.class);


  private static final String tokenHeader = "Authorization";

  protected FilterAuthenticationJwt(RequestMatcher requiresAuthenticationRequestMatcher) {
    super(requiresAuthenticationRequestMatcher);
  }

  protected FilterAuthenticationJwt(String defaultFilterProcessesUrl) {
    super(defaultFilterProcessesUrl);
  }

  @Override
  public void setAuthenticationManager(AuthenticationManager authenticationManager) {
    super.setAuthenticationManager(authenticationManager);
  }

  @Override
  public Authentication attemptAuthentication(HttpServletRequest servletRequest,
      HttpServletResponse response) throws AuthenticationException, IOException, ServletException {

    logger.debug("new doFilter: {}", servletRequest.getPathInfo());
    // Get token
    String authorization = servletRequest.getHeader(tokenHeader);

    if (authorization == null) {
      throw new JwtTokenMissingException("No JWT token found in request headers");
    }
    logger.trace("Authorization found on Header");

    // String replace to accept both oauth with 'bearer' or general JWT token
    UserDetails user = JwtUtility.parseToken(authorization.replaceAll("Bearer ", ""));
    // get user credentials
    Authentication credentials = new UsernamePasswordAuthenticationToken(user.getUsername(),
        user.getPassword(), user.getAuthorities());

    return credentials;
  }

  @Override
  protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response,
      FilterChain chain, Authentication authResult) throws IOException, ServletException {
    super.successfulAuthentication(request, response, chain, authResult);
    chain.doFilter(request, response);
  }

}
