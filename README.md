## Introduction
The application is simple RESTful API which provides a service for storing, updating,
retrieving and deleting Person entities.


## Running the application
You can run the application in the following ways:  
1) Test version running on Amazon AWS server:  
    http://emdbspringboottest-env.mwfpmnsagd.us-east-1.elasticbeanstalk.com/swagger-ui.html    
2) You can clone the repository and import to the Eclipse IDE, and use Maven to create the executable Springboot .jar  


## Using the application  
More details on the commands can be found on:    
http://emdbspringboottest-env.mwfpmnsagd.us-east-1.elasticbeanstalk.com/swagger-ui.html  

1) User Authentication
The application uses JWT Token for user authentication and it must be added to all functionalities in the header "Authorization"
For getting the token:  

Method:  
- POST   
Headers:   
- Content-type: application/json  
URL:   
- http://emdbspringboottest-env.mwfpmnsagd.us-east-1.elasticbeanstalk.com/login  
Body:   
{"username": "admin","password": "admin"}  

Response Header example:  
- Token eyJhbGciOiJIUzI1NiJ9.eyJ1c3IiOiJ7XCJ1c2VybmFtZVwiOlwiYWRtaW5cIixcImF1dGhvcml0aWVzXCI6W3tcInJvbGVcIjpcIlJPTEVfQURNSU5cIn1dLFwiYWNjb3VudE5vbkV4cGlyZWRcIjp0cnVlLFwiYWNjb3VudE5vbkxvY2tlZFwiOnRydWUsXCJjcmVkZW50aWFsc05vbkV4cGlyZWRcIjp0cnVlLFwiZW5hYmxlZFwiOnRydWV9IiwiaXNzIjoiZ2ZtZWxsby5lbWRiIiwic3ViIjoiYWRtaW4iLCJleHAiOjE1MjgxMjIzNjB9.5_8ixgbBg3q7LUB19pO1353HPF827dDKcsu68Pn  

2) Creating new Person  
Method:  
- POST   
Headers:   
- Content-type: application/json  
- Authorization: {token value}  
URL: http://emdbspringboottest-env.mwfpmnsagd.us-east-1.elasticbeanstalk.com/person  
Body:   
{
    "idPerson": 3,
    "age": 2,
    "first_name": "John",
    "last_name": "Roberts",
    "favourite_colour": "green"
}

3) Creating Multiple New Person. This is an assynchronous service.  
Method:  
- POST   
Headers:   
- Content-type: application/json  
- Authorization: {token value}  
URL: http://emdbspringboottest-env.mwfpmnsagd.us-east-1.elasticbeanstalk.com/person/batch  
Body:   
{
   "person":[
      {
         "first_name":"John",
         "last_name":"Keynes",
         "age":"29",
         "favourite_colour":"red"
      },
      {
         "first_name":"Sarah",
         "last_name":"Robinson",
         "age":"54",
         "favourite_colour":"blue"
      }
   ]
}

4) Updating existing Person  
Method:  
- PUT   
Headers:   
- Content-type: application/json  
- Authorization: {token value}  
URL: http://emdbspringboottest-env.mwfpmnsagd.us-east-1.elasticbeanstalk.com/person/{idPerson}  
Body:   
{
    "age": 2,
    "first_name": "John New Name",
    "last_name": "Roberts",
    "favourite_colour": "green"
}

4) Updating some fields of existing Person  
Method:  
- PATCH   
Headers:   
- Content-type: application/json  
- Authorization: {token value}  
URL: http://emdbspringboottest-env.mwfpmnsagd.us-east-1.elasticbeanstalk.com/person/{idPerson}  
Body:   
{
    "last_name": "Roberts New Lastname"
}

5) Delete existing Person  
Method:  
- DELETE   
Headers:   
- Authorization: {token value}  
URL: http://emdbspringboottest-env.mwfpmnsagd.us-east-1.elasticbeanstalk.com/person/{idPerson}  
Body:   

6) Find all Persons  
Method:  
- GET   
Headers:   
- Authorization: {token value}  
URL: http://emdbspringboottest-env.mwfpmnsagd.us-east-1.elasticbeanstalk.com/person  

7) Find Person by Id  
Method:  
- GET   
Headers:   
- Authorization: {token value}  
URL: http://emdbspringboottest-env.mwfpmnsagd.us-east-1.elasticbeanstalk.com/person/{idPerson}  
